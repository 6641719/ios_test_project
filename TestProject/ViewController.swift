//
//  ViewController.swift
//  TestProject
//
//  Created by Amir on 21.02.2022.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var addChildButton: UIButton!
    @IBOutlet weak var clearChildButton: UIButton!
    @IBOutlet weak var nameSection: UIStackView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var ageSection: UIStackView!
    
    let tableViewData = Array(repeating: "Item", count: 3)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addChildButton = buttonChanger(addChildButton, .systemBlue, UIColor.systemBlue.cgColor)
        clearChildButton = buttonChanger(clearChildButton, .red, UIColor.red.cgColor)
        clearChildButton.layer.borderColor = UIColor.red.cgColor
        nameSection = stackViewBorderCreater(nameSection)
        ageSection = stackViewBorderCreater(ageSection)
        
        registerTableViewCells()
        
        tableView.dataSource = self

    }
    
    
    func stackViewBorderCreater(_ stackView: UIStackView) -> UIStackView{
        stackView.layer.borderColor = UIColor.lightGray.cgColor
        stackView.layer.borderWidth = 1
        stackView.layer.cornerRadius = 5
        return stackView
    }
    func buttonChanger(_ button: UIButton, _ color: UIColor, _ borderColor: CGColor) -> UIButton{
        button.backgroundColor = .clear
        button.tintColor = color
        button.layer.cornerRadius = 28
        button.layer.borderWidth = 2
        button.layer.borderColor = borderColor
        return button
    }
    
    private func registerTableViewCells() {
        let textFieldCell = UINib(nibName: K.cellNibName,
                                  bundle: nil)
        tableView.register(textFieldCell,
                           forCellReuseIdentifier: K.cellIdentifier)
    }
}

extension ViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: K.cellIdentifier, for: indexPath) as! CustomTableViewCell
        cell.nameLabel.text = self.tableViewData[indexPath.row]
        cell.ageLabel.text = "15"
        return cell
    }
}
