//
//  ButtonConfiguration.swift
//  TestProject
//
//  Created by Amir on 21.02.2022.
//

import UIKit

class ButtonConfiguration: UIButton {
    
    override init(frame: CGRect) {
           super.init(frame: frame)
           backgroundColor = .clear
           layer.cornerRadius = 18
           layer.borderWidth = 2
           layer.borderColor = UIColor.blue.cgColor
       }

    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
