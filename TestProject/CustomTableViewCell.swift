//
//  CustomTableViewCell.swift
//  TestProject
//
//  Created by Amir on 21.02.2022.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UITextField!
    @IBOutlet weak var ageLabel: UITextField!
    
    @IBOutlet weak var ageSection: UIStackView!
    @IBOutlet weak var nameSection: UIStackView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ageSection = stackViewBorderCreater(ageSection)
        nameSection = stackViewBorderCreater(nameSection)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func stackViewBorderCreater(_ stackView: UIStackView) -> UIStackView{
        stackView.layer.borderColor = UIColor.lightGray.cgColor
        stackView.layer.borderWidth = 1
        stackView.layer.cornerRadius = 5
        return stackView
    }
    
}
